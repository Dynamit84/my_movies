import React from 'react';
import { render } from 'react-dom';
import App from './javascript/components/App';
import './styles/styles.less';
import './images/appBg.jpg';
import { createStore, applyMiddleware } from 'redux';
import { allReducers } from './javascript/reducers/combine-reducers';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import  { createLogger } from 'redux-logger';
import Mongoose from 'mongoose';

Mongoose.connect('localhost:27017/movies');

const store = createStore(allReducers, applyMiddleware(thunk, createLogger()));

render(<Provider store={store}><App /></Provider>, document.getElementById('app'));

