import  React from 'react';

export default class CustomSelect extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {
			isClosed: true
		};
	}
	
	openCustomSelect(e, selectId) {
		this.setState({isClosed: false});
	}

	closeCustomSelect() {
		this.setState({isClosed: true});
	}
	
	render() {
		let config = this.props.config,
		openClass = this.state.isClosed ? "":" opened",
		configId = config.id;
		
		return (
			<div className="selectWrapper">
				<div className={"selectCustom" + openClass}
					 id={`${configId}Title`}
					 onClick={() => {this.openCustomSelect()}}>{config.text}</div>
				<ul className="select" id={config.id} onClick={(e) => this.stopPropagation(e)}>
					{config.array.map( (elem, index) =>
						<li className="selectItem" key={index}><label><input type="checkbox"/>{elem}</label></li>
					)}
				</ul>
			</div>
		);
	}
}