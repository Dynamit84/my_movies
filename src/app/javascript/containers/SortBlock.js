import React from 'react';
import { sortByTitles, sortByTitlesReverse, sortByNumbers, highLightActive } from '../actions/actions';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

const sortArr = [
	{
		id: 'sortTitle',
		sortAttr: 'title',
		value: 'A-Z'
	},
	{
		id: 'sortTitleReverse',
		sortAttr: 'title',
		value: 'Z-A'
	},
	{
		id: 'sortYear',
		sortAttr: 'releaseYear',
		value: 'Year'
	},
	{
		id: 'sortRating',
		sortAttr: 'myRating',
		value: 'Most rating'
	},
	{
		id: 'sortIMDB',
		sortAttr: 'imdb',
		value: 'Top IMDb'
	}
];

class SortBlock extends React.Component {
	
	clickHandler (attr, id) {
		let func;
		const { sortByTitles, sortByTitlesReverse, sortByNumbers } = this.props;
		
		switch (id) {
			
			case 'sortTitle':
				func = sortByTitles;
				break;
			
			case 'sortTitleReverse':
				func = sortByTitlesReverse;
				break;
			
			default:
				func = sortByNumbers;
		}
		
		func({ attr, id });
	};
	
	render() {

		const { activeId } = this.props;

		return (
			<div className="sort-container">
				<span className="sort-title">Sort by:</span>
				<ul>
					{
						sortArr.map((el, i) => {
							let
								{sortAttr, id, value} = el,
								classes = id === activeId ? 'sort active': 'sort';
							
							return (
								<li id={id}
									className={classes}
									key={i}
									onClick={this.clickHandler.bind(this, sortAttr, id)}
								>{value}</li>
							);
						})
					}
				</ul>
			</div>
		)
	}
}

function mapStateToProps(state) {

	return {
		activeId: state.MoviesReducer.activeId
	};
}

function mapDispatchToProps(dispatch) {

	return bindActionCreators({ sortByTitles, sortByTitlesReverse, sortByNumbers, highLightActive }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SortBlock);