import React from 'react';
import { Link } from 'react-router-dom';
import FiltersBlock from'../components/FiltersBlock';
import SortBlock from'./SortBlock';
import Rating from '../components/Rating';
import { connect } from 'react-redux';

class MoviesList extends React.Component {

	render() {
		const { visibleMovies } = this.props;

		return (
			<div className="gallery-container">
				<div className="filter-sort-container">
					<FiltersBlock />
					<SortBlock />
				</div>
				<ul className="gallery">
					{visibleMovies.map( (movie, index) =>
						<li key={index} className="movie-item">
							<Link to={'films/' + movie.id} className="movie-link">
								<img src={movie.image} alt=""/>
								<div className="title-wrapper">
									<p className="movie-title">{movie.title}</p>
								</div>
							</Link>
							<div className="popup-container">
								<div className="popup-header">
									<p>{movie.title}</p>
									<Rating rating={movie.myRating} />
								</div>
								<div className="popup-body">
									<div className="gray-block">
										<p className="imdb">IMDb {movie.imdb}</p>
										<span>{movie.releaseYear}</span>
										<span>{movie.duration}</span>
									</div>
									<div className="p-wrapper">
										<p className="description">{movie.storyLine}</p>
										<p><span>Country: </span>{movie.country.join(', ')}</p>
										<p><span>Genre: </span>{movie.genres.join(', ')}</p>
									</div>
								</div>
							</div>
						</li>
					)}
				</ul>
			</div>
		);
	}
}

function mapStateToProps(state) {

	return {
		visibleMovies: state.MoviesReducer.filteredMovies
	};
}

export default connect(mapStateToProps)(MoviesList);