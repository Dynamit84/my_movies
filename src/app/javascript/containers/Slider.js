import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

class Slider extends React.Component {
	
	render() {
		const { movies } = this.props;

		return (
			<div className="images-container">
				{movies.map((movie, i) => {
					
						if(i === 0) {

							return <Link to={'films/' + movie.id} key={i} className="first-slide"><img src={movie.image} alt=""/></Link>;
						} else {
							return <Link to={'films/' + movie.id} key={i}><img src={movie.image} alt=""/></Link>;
						}
					}
				)}
				{movies.slice(0,5).map((movie, i) => {
						
						return <Link to={'films/' + movie.id} key={i}><img src={movie.image} alt=""/></Link>;
					}
				)}
			</div>
		);
	}
}

function mapStateToProps(state) {

	return {
		movies: state.SliderReducer
	};
}

export default connect(mapStateToProps)(Slider);
