import React from 'react';
import Slider from './Slider';
import MoviesList from './MoviesList';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { fetchMovies } from '../actions/actions';
import { Loader } from '../components/Loader';

class Home extends React.Component {

	componentDidMount() {
		let { fetchMovies } = this.props;

		fetchMovies();
	}
	
    render() {

		const { isLoading } = this.props;

		if(isLoading) {

			return <Loader />
		}

		/*const { movies } = this.props;

		if(movies.length === 0) {

			return <p className="no-movies-msg">To start your own movies catalogue, please choose menu item "Add Movie" and fill all required fields.</p>
		}*/

        return (
        	<div className="homepage-container">
				<Slider />
				<MoviesList />
			</div>
        );
    }

}

function mapStateToProps(state) {

	return {
		movies: state.MoviesReducer.filteredMovies,
		isLoading: state.MoviesReducer.isLoading
	};
}

function mapDispatchToProps(dispatch) {
	
	return bindActionCreators({ fetchMovies }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);