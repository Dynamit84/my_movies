export default function (state = [], action) {
	const {type, payload} = action;
	const newState = [...state];
	
	switch (type) {
		case 'FETCH_MOVIES_SUCCESS':

			return populateSlider(newState, payload);
		
		default:
			
			return state;
	}
}

const populateSlider = (state, movies) => {


	if(movies.length < 7) {

		return state;
	}

	let uniqueMovies = generateUnique(movies.length);
	
	state = uniqueMovies.map((movieNum) => movies[movieNum]);
	
	return state;
};

let generateUnique = (max) => {

	let numArray =  [...Array(max-1).keys()];
	
	return shuffleArray(numArray).slice(0,7);
};

let shuffleArray = (arr) => {
	let currentIndex = arr.length, randomIndex;
	
	while ( currentIndex !== 0 ) {
		
		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex--;
		
		// And swap it with the current element.
		[arr[currentIndex], arr[randomIndex]] = [arr[randomIndex], arr[currentIndex]]
	}
	
	return arr;
};