export default function (state = { isOpened: false }, action) {
	const {type, payload} = action;
	const newState = Object.assign({}, state);

	switch (type) {
		case 'OPEN_CLOSE_FILTER':

			newState.isOpened = !newState.isOpened;

			return  newState;
		default:

			return state;
	}
}