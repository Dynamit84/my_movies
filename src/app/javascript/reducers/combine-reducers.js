import { combineReducers } from 'redux';
import MoviesReducer from './reducer-movies';
import SliderReducer from './reducer-slider';
import FilterReducer from './reducer-filter';

export const allReducers = combineReducers({
    MoviesReducer,
	SliderReducer,
	FilterReducer
});
