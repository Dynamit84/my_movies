export default function (state = { movies: [], filteredMovies: [], activeId: '' }, action) {
    const { type, payload } = action;

	switch (type) {
        case 'MOVIES_ARE_LOADING':

            return Object.assign({}, state, {isLoading: payload});
        case 'FETCH_MOVIES_SUCCESS':

            return Object.assign({}, state, {movies: payload, filteredMovies: payload});
        case 'SORT_TITLES_REVERSE':

            return Object.assign({}, state, {filteredMovies:sortTitles([...state.filteredMovies], payload.attr).reverse(),
                                    activeId: payload.id});
        case 'SORT_TITLES':

            return Object.assign({}, state, {filteredMovies:sortTitles([...state.filteredMovies], payload.attr),
                                    activeId: payload.id});
        case 'SORT_NUMBERS':

            return Object.assign({}, state, {filteredMovies:sortNumbers([...state.filteredMovies], payload.attr),
                                    activeId: payload.id});
		case 'SET_VISIBILITY_FILTER':
			
			return Object.assign({}, state, {filteredMovies:getVisibleMovies([...state.filteredMovies], payload)});
        default:
            return state;
    }
}

const sortNumbers = (array, sortAttr) => {

    return array.sort((a, b) => {
        if(b[sortAttr] === '-') {
            b[sortAttr] = 0;
        }

        if(a[sortAttr] === '-') {
            a[sortAttr] = 0;
        }

        return b[sortAttr] - a[sortAttr];
    });
};

const sortTitles = (array, sortAttr) => {

    return array.sort((a, b) => {
        let title1 = a[sortAttr],
            title2 = b[sortAttr];

        if(title1 < title2) {

            return -1;
        }

        if(title1 > title2) {

            return 1;
        }

        return 0;
    });
};

const getVisibleMovies = (movies, filterData) => {
	const { filter, value } = filterData;
	
	switch (filter) {
		case 'SHOW_ALL':
			
			return movies;
		case 'SHOW_DATE_HIGHER':
			
			return movies.filter(movie => movie['releaseYear'] > value);
		case 'SHOW_DATE_LOWER':
			
			return movies.filter(movie => movie['releaseYear'] < value);
		case 'SHOW_IMDB_HIGHER':
			
			return movies.filter(movie => movie['imdb'] > value);
		case 'SHOW_IMDB_LOWER':
			
			return movies.filter(movie => movie['imdb'] < value);
		case 'SHOW_MYRATING_HIGHER':
			
			return movies.filter(movie => movie['myRating'] > value);
		case 'SHOW_MYRATING_LOWER':
			
			return movies.filter(movie => movie['myRating'] < value);
	}
};

