import React from 'react';
import { Search } from './Search';
import { NavLink } from 'react-router-dom';

export default class Header extends React.Component{

	render() {
		
		return(
			<div className="header-container">
				<div className="content">
					<div id="cssmenu" className="dropdown">
						<div className="logo">
							<NavLink to={'/home'}>
								<img src="/app/images/My_Movies_Zone_Logo_Light.png" alt=""/>
							</NavLink>
						</div>
						<ul>
							<li><NavLink to={'/home'} activeClassName={'active'}>Home</NavLink></li>
							<li><NavLink to={'/add'} activeClassName={'active'}>Add Movie</NavLink></li>
							<li><NavLink to={'/about'} activeClassName={'active'}>About</NavLink></li>
						</ul>
						<Search />
					</div>
				</div>
			</div>
		);
	}
}
