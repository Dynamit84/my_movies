import React from 'react';
import { Link } from 'react-router-dom';

export const NotFound = () => {

    return (
        <div className="not-found">
            <div className="text-wrapper">
                <div>
                    <img className="simpson" src="app/images/simpson.png" alt="Not Found"/>
                    <div className="wrapper-404">
                        <span>4</span><img className="donut" src="app/images/donut.png" alt="0"/><span>4</span>
                    </div>
                </div>
                <div>
                    <p className="error">Error!</p>
                    <p className="page-not-found">Page Not Found</p>
                    <p className="error-text">For Some Reason The Page You Requested Could Not Be Found</p>
                    <div >
                        <a href="javascript:history.go(-1)">« Go Back</a> / <Link to={'home'}>Go Home »</Link>
                    </div>
                </div>
            </div>
        </div>
    );
};
