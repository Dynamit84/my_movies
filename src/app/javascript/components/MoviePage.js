import React from 'react';
import movies from '../../moviesDB/movies.json';

export default class MoviePage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            movie: this.findMovie()
        }
    }

    findMovie() {

        return movies.find((movie) => {

            return movie['id'] === this.props.match.params.id;
        });
    }

    render() {
        let movie = this.state.movie;

        return (
            <div className="movie-wrapper">
                <h1>{movie.title}</h1>
				<div className="info-wrapper">
					<img src={'../' + movie.image} alt=""/>
					<div className="text-container">
						<ul className="text-info">
							<li><span>Release:</span> {movie.releaseYear}</li>
							<li><span>Country:</span> {movie.country.join(', ')}</li>
							<li><span>Stars:</span> {movie.stars.join(', ')}</li>
							<li><span>Director:</span> {movie.director.join(', ')}</li>
							<li><span>Genre:</span> {movie.genres.join(', ')}</li>
							<li><span>Budget:</span> {movie.budget}</li>
							<li><span>Duration:</span> {movie.duration}</li>
						</ul>
						<p>{movie.storyLine}</p>
					</div>
				</div>
            </div>
        );
    }
}
