import React from 'react';
import Header from './Header';
import Home from '../containers/Home';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import AddMovie from './AddMovie';
import { About } from './About';
import MoviePage  from './MoviePage';
import { NotFound } from './GenericNotFound';

export default class App extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {
			isScrolled: false
		};
	}
	
	handleScroll() {
		if (document.body.scrollTop >= 400) {
			this.setState({isScrolled: true});
		} else {
			this.setState({isScrolled: true});
		}
	}

	scrollToTop(duration) {
		
		if (duration < 0) return;
		let difference = 0 - document.body.scrollTop;
		let perTick = difference / duration * 2;
		
		setTimeout(() => {
			document.body.scrollTop = document.body.scrollTop + perTick;
			this.scrollToTop(duration -= 2);
		}, 10);
	}
	
    render() {
		let qwe = this.state.isScrolled ? 'scrolled': '';
		let classes = `main-container ${qwe}`;
		
        return (
			<BrowserRouter>
				<div className={classes}>
					<Header />
					<div className="page-content">
						<Switch>
							<Route exact={true} path='/' component={Home} />
							<Route path='/home' component={Home} />
							<Route path='/add' component={AddMovie} />
							<Route path={'/about'} component={About} />
							<Route path={'/films/:id'} component={MoviePage} />
							<Route component={NotFound} />
						</Switch>
					</div>
					<div className="arrow-up" onClick={() => this.scrollToTop(150)}>
						<img src="/app/images/top-arrow.png" alt=""/>
					</div>
				</div>
			</BrowserRouter>
        );
    }
}
