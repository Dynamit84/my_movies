import React from 'react';
import CustomSelect from '../containers/CustomSelect';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux';
import { openCloseFilter, setVisibilityFilter } from '../actions/actions';

class FiltersBlock extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {
			countries: this.getState('country'),
			genres: this.getState('genres')
		}
	}
	
	getState(field) {
		let state = [];
		const { movies } = this.props;
		
		movies.forEach((movie) =>
			movie[field].forEach( (item) => {
				if(!state.includes(item)) {
					state.push(item)
				}
			})
		);
		
		return state.sort();
	}
	
	selectWithYears(classValue, filter) {
		const year = new Date().getFullYear(),
			{ setVisibilityFilter } = this.props;

		return (
			<select onChange={(e) => setVisibilityFilter({filter, value: e.target.value})} className={classValue}>
				<option value="">-</option>
				{[...Array(48)].map((x, i) => {
						
						return (
							<option value={year-i} key={i}>{year-i}</option>
						);
					}
				)}
			</select>
		);
	}
	
	selectWithRating(classValue, num, filter) {
		const { setVisibilityFilter } = this.props;

		return (
			<select onChange={(e) => setVisibilityFilter({filter, value: e.target.value})} className={classValue}>
				<option value="">-</option>
				{[...Array(num)].map((x, i) => {
						
						return (
							<option value={i+1} key={i}>{i + 1}</option>
						);
					}
				)}
			</select>
		);
	}
	
	disableValues(target, classValue, value) {
		let parentEl = target.parentElement,
			elemToDisable = parentEl.querySelector(classValue),
			optionsEl =elemToDisable.querySelectorAll('option'),
			sign = classValue === '.from'? 'disableHigher': 'disableLower';
		
			Array.prototype.slice.call(optionsEl).forEach((elem) => this[sign](elem));
	}
	
	disableHigher(elem) {
		if (elem.getAttribute('value') > value) {
			elem.setAttribute('disabled', 'disabled');
		}
	}
	
	disableLower(elem) {
		if (elem.getAttribute('value') < value) {
			elem.setAttribute('disabled', 'disabled');
		}
	}
	
	render() {
		
		const { filterState, openCloseFilter } = this.props;
		
		if (!filterState.isOpened) {
			
			return (
				<div className="filters-container-collapsed">
					<a className="orange-link" href="javascript:void(0);" onClick={() => openCloseFilter()}>Expand filters</a>
				</div>
			)
		}
		
		return (
			<div className="filters-container">
				<input type="button" className="close" value="&#10006;" onClick={() => openCloseFilter()}/>
				<span className="filters-title">Filters</span>
				<div className="filters-wrapper">
					<CustomSelect config={{id: 'genreList', text: 'Genre', array: this.state.genres}}/>
					<CustomSelect config={{id: 'countryList', text: 'Country', array: this.state.countries}}/>
					<div className="release-wrapper">
						<span className="block">Release Date:</span>
						<div className="input-wrapper">
							from {this.selectWithYears('dateFrom', 'SHOW_DATE_HIGHER')}
							to {this.selectWithYears('dateTo', 'SHOW_DATE_LOWER')}
						</div>
					</div>
					<div className="imdb-rating-wrapper">
						<span className="block">IMDb Rating:</span>
						<div className="input-wrapper">
							from {this.selectWithRating('imdbFrom', 10, 'SHOW_IMDB_HIGHER')}
							to {this.selectWithRating('imdbTo', 10, 'SHOW_IMDB_LOWER')}
						</div>
					</div>
					<div className="my-rating-wrapper">
						<span className="block">My Rating:</span>
						<div className="input-wrapper">
							from {this.selectWithRating('myRatingFrom', 5, 'SHOW_MYRATING_HIGHER')}
							to {this.selectWithRating('myRatingTo', 5, 'SHOW_MYRATING_LOWER')}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

function mapStateToProps(state) {

	return {
		filterState: state.FilterReducer,
		movies: state.MoviesReducer.movies
	};
}

function mapDispatchToProps(dispatch) {
	
	return bindActionCreators({ openCloseFilter, setVisibilityFilter }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(FiltersBlock);