import React from 'react';
import CustomSelect from '../containers/CustomSelect';
import Rating from './Rating';

const COUNTRIES = [
		"Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola",
		"Anguilla", "Argentina", "Armenia", "Aruba", "Australia", "Austria",
		"Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin",
		"Bermuda", "Bhutan", "Bolivia ", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei Darussalam",
		"Bulgaria", "Burkina Faso",
		"Burundi", "Cabo Verde", "Cambodia", "Cameroon", "Canada", "Central African Republic",
		"Chad", "Chile", "China", "Christmas Island", "Colombia", "Comoros", "Congo",
		"Congo (DR)", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba",
		"Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "Ecuador",
		"Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands",
		"Finland", "France",
		"Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece",
		"Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guernsey", "Guinea", "Guinea-Bissau", "Guyana",
		"Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland",
		"India", "Indonesia", "Iran", "Iraq", "Ireland", "Isle of Man", "Israel", "Italy", "Jamaica", "Japan",
		"Jersey", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea (South)",
		"Korea (North)", "Kuwait", "Kyrgyzstan", "Latvia", "Lebanon",
		"Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macao", "Macedonia", "Madagascar",
		"Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Martinique", "Mauritania", "Mauritius",
		"Mayotte", "Mexico", "Micronesia", "Moldova", "Monaco", "Mongolia",
		"Montenegro", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands",
		"New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island",
		"Norway", "Oman", "Pakistan", "Palau", "Palestine, State of", "Panama",
		"Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico",
		"Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Barthelemy",
		"San Marino", "Saudi Arabia", "Senegal", "Serbia", "Seychelles", "Sierra Leone",
		"Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa",
		"South Sudan", "Spain", "Sri Lanka", "Sudan", "Suriname",
		"Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic",
		"Taiwan", "Tajikistan", "Tanzania", "Thailand", "Timor-Leste",
		"Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan",
		"Tuvalu", "Uganda", "Ukraine", "United Arab Emirates",
		"United Kingdom", "USA", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Viet Nam",
		"Western Sahara", "Yemen",
		"Zambia", "Zimbabwe"
	],
	GENRES = [
		"Action",
		"Adventure",
		"Animation",
		"Biography",
		"Comedy",
		"Costume",
		"Crime",
		"Documentary",
		"Drama",
		"Family",
		"Fantasy",
		"History",
		"Horror",
		"Kungfu",
		"Musical",
		"Mystery",
		"Mythological",
		"Psychological",
		"Romance",
		"Sci-Fi",
		"Sitcom",
		"Sport",
		"Thriller",
		"TV Show",
		"War",
	];

export default class Add_Movie extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {
			openedCS: ''
		}
	}
	
	selectWithYears() {
		let year = new Date().getFullYear();
		
		return (
			<select id="years">
				<option value="">-</option>
				{[...Array(48)].map((x, i) => {
					
						return (
							<option value={year-i} key={i}>{year-i}</option>
						);
					}
				)}
			</select>
		);
	}
	
	handleCSInteractions(selectId) {
		this.setState({openedCS: selectId});
	}

    render () {
	
		return (
			<div className="form-container">
				<form id="saveMovie" method="post">
					<div className="flex-wrapper">
						<label htmlFor="title" className="fix-width">Movie Title</label>
						<input id="title" type="text" placeholder="Title"/>
					</div>
					<div className="flex-wrapper">
						<label htmlFor="stars" className="fix-width">Main Stars</label>
						<input id="stars" type="text" placeholder="Stars"/>
						<input type="button" className="addFiled"/>
					</div>
					<div className="flex-wrapper">
						<label htmlFor="director" className="fix-width">Director</label>
						<input id="director" type="text" placeholder="Director"/>
					</div>
					<div className="flex-wrapper space-between">
						<CustomSelect config={
								{
									id: 'countryList',
									text: 'Country',
									array: COUNTRIES,
									isClosed: this.state.openedCS === this.id
								}
							}
						/>
						<CustomSelect config={
								{
									id: 'genreList',
									text: 'Genre',
									array: GENRES,
									isClosed: this.state.openedCS === this.id
								}
							}
						/>
					</div>
					<div className="rating-wrapper">
						<span>Your Rating </span>
						<Rating dynamic={true}/>
					</div>
					<div className="flex-wrapper space-between">
						<div className="flex-wrapper">
							<label htmlFor="duration">Duration</label>
							<input type="number" min='0' id="duration"/>
						</div>
						<div className="flex-wrapper">
							<label htmlFor="imdb">IMDb</label>
							<input type="number" id="imdb" min='0' max='10'/>
						</div>
						<div className="flex-wrapper">
							<label htmlFor="budget">Budget</label>
							<input type="number" id="budget" min='0'/>
						</div>
						<div className="flex-wrapper">
							<label htmlFor="years">Year</label>
							{this.selectWithYears()}
						</div>
					</div>
					<div className="btn-wrapper">
						<label htmlFor="pic" className="upload">Upload Poster</label>
						<input type="file" id="pic" accept="image/png"/>
					</div>
					<textarea id="storyline" cols="20" rows="10" placeholder="Storyline"></textarea>
					<div className="btn-wrapper">
						<button name="submit" type="submit" id="movie-submit"><span>Add Movie</span></button>
					</div>
				</form>
			</div>
		);
	}
};
