import  React from 'react';

const RATING = ['bad', 'bored', 'fine', 'good', 'excellent'];

export default class Rating extends React.Component {
	
	addGold(e) {
		let stars = document.body.querySelectorAll('.rating > .star'),
			starIndex = e.target.getAttribute('data-key'),
			starText = e.target.getAttribute('data-rating'),
			textRating = document.body.querySelector('.text-rating span');
		
		Array.prototype.forEach.call(stars, function (elem, i) {
			if(i <= starIndex) {
				
				elem.classList.add('gold');
			}
		});
		
		textRating.textContent = starText;
		textRating.className = '';
		textRating.classList.add(starText);
	}
	
	removeGold(e) {
		let stars = document.querySelectorAll('.rating > .star'),
			starIndex = e.target.getAttribute('data-key'),
			currRating = document.querySelectorAll('.rating > .yellow').length,
			textRating = document.body.querySelector('.text-rating span');
		
		Array.prototype.forEach.call(stars, function (elem, i) {
			if(i <= starIndex) {
				
				elem.classList.remove('gold');
			}
		});
		
		if(!currRating) {
			textRating.textContent = 'not rated';
			textRating.className = '';
		} else {
			textRating.textContent = RATING[currRating - 1];
			textRating.className = RATING[currRating - 1];
		}
	}
	
	addRating(e) {
		let stars = document.querySelectorAll('.rating > .star'),
			starIndex = e.target.getAttribute('data-key'),
			currRating = document.querySelectorAll('.rating > .yellow').length,
			forEach	= Array.prototype.forEach;
		
		if(currRating && currRating !== starIndex) {
			forEach.call(stars, function (elem) {
				
				elem.classList.remove('yellow');
				
			});
		}
		
		forEach.call(stars, function (elem, i) {
			if(i <= starIndex) {
				
				elem.classList.add('yellow');
			}
		});
	}
	
	render() {
		switch (true) {
			case this.props.rating === '-':
				
				return <p className="no-rating">Not rated</p>;
			
			case this.props.dynamic:
				
				return (
					<div>
						<div className="rating">
							{[...Array(5)].map((x, i) => {
									let classes = 'star';
									
									return (
										<span
											key={i}
											data-key={i}
											data-rating={RATING[i]}
											onMouseEnter={(e) => this.addGold(e)}
											onMouseLeave={(e) => this.removeGold(e)}
											onClick={(e) => this.addRating(e)}
											className={classes}>
								</span>
									);
								}
							)}
						</div>
						<div className="text-rating">
							<span>Not Rated</span>
						</div>
					</div>
				);
		}
		
		return (
			<div className="rating">
				{[...Array(5)].map((x, i) => {
						let classes = 'star';
						
						if(i < this.props.rating) {
							classes +=' yellow'
						}
						
						return (
							<span key={i} className={classes}></span>
						);
					}
				)}
			</div>
		);
	}
};
