import React from 'react';

export const Search = (props) => {

    return (
        <div className="search">
            <form className="search-form">
                <input type="text" placeholder="Enter movie title..." required/>
                <button className="search-btn"><span>Search</span></button>
            </form>
        </div>
    );
};
