import React from 'react';

export const About = () => {

    return (
        <div className="about-container">
            <h1>My Movies Zone</h1>
            <p>"My Movies Zone" helps you to catalog your collection or track which movies you've watched,
                "My Movies Zone" has the tools you need to begin building your own movie database.</p>
            <h2>"My Movies Zone" Helps You:</h2>
            <ul>
                <li>Catalogue your movie collection</li>
                <li>Organize all information about watched movies</li>
                <li>Store extended cast info</li>
                <li>Track your collection through detailed statistics</li>
                <li>Add your own own comments and ratings after watching a film</li>
                <li>Become a movie expert!</li>
            </ul>
            <div className="form-container">
                <form id="contact" action="" method="post">
                    <h3>Contact Me</h3>
                    <fieldset>
                        <input placeholder="Your Name" type="text" tabIndex="1" required autoFocus/>
                        <input placeholder="Your Email Address" type="email" tabIndex="2" required/>
                    </fieldset>
                    <fieldset>
                        <textarea placeholder="Type your Message Here..." tabIndex="3" required/>
                    </fieldset>
                    <div className="btn-wrapper">
                        <button name="submit" type="submit" id="contact-submit"><span>Send</span></button>
                    </div>
                </form>
            </div>
        </div>
    );
};