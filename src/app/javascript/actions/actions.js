import movies from '../../moviesDB/movies.json';

export const sortByTitles = (sortAttr) => {

    return {
        type: 'SORT_TITLES',
        payload:sortAttr
    }
};

export const sortByTitlesReverse = (sortAttr) => {

    return {
        type: 'SORT_TITLES_REVERSE',
        payload: sortAttr
    }
};

export const sortByNumbers = (sortAttr) => {

    return {
        type: 'SORT_NUMBERS',
        payload: sortAttr
    }
};

export const openCloseFilter = () => {

    return {
        type: 'OPEN_CLOSE_FILTER'
    }
};

export const setVisibilityFilter = (filterData) => {

   return {
       type: 'SET_VISIBILITY_FILTER',
       payload: filterData
   }
};

export const fetchMovies = () => (dispatch) => {
	
	dispatch(moviesAreLoading(true));
	
	promise
		.then((movies) => {
			dispatch(moviesAreLoading(false));
			
			return movies;
		})
		.then((movies) => {
			dispatch(moviesFetchSuccess(movies));
		})
        .catch(()=> dispatch(moviesFetchHasErrored(true)));
};

let promise = new Promise((resolve) => {
	setTimeout(() => resolve(movies), 1000);
});

export const moviesFetchHasErrored = (bool) => {
	
	return {
		type: 'FETCH_MOVIES_HAS_ERRORED',
		payload: bool
	};
};

export const moviesAreLoading = (bool) => {
	
	return {
		type: 'MOVIES_ARE_LOADING',
		payload: bool
	};
};

export const moviesFetchSuccess = (movies) => {
	
    return {
        type: 'FETCH_MOVIES_SUCCESS',
        payload: movies
    };
};


