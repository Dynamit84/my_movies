import Mongoose from 'mongoose';

const Schema = Mongoose.Schema;

const schema = new Schema({
	id: {type: String, require: true},
	title: {type: String, require: true},
	image: {type: String, require: true},
	stars: {type: Array, require: true},
	storyLine: {type: String, require: true},
	director:{type: Array, require: true},
	country: {type: Array, require: true},
	duration: {type: String, require: true},
	budget: {type: String, require: true},
	myRating: {type: Number, require: true},
	imdb: {type: Number, require: true},
	releaseYear: {type: Number, require: true},
	genres: {type: Array, require: true}
});

export default Mongoose.model('Movies', schema);