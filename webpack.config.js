var webpack = require('webpack'),
	path = require('path'),
	DIST_DIR = path.resolve(__dirname, 'dist'),
	SRC_DIR = path.resolve(__dirname, 'src'),
	ExtractTextPlugin = require('extract-text-webpack-plugin');


console.log('entry', path.join(SRC_DIR, '/app/index.js'));
console.log('output path',path.join(DIST_DIR, '/app'));

module.exports = {
	entry: path.join(SRC_DIR, '/app/index.js'),
	output: {
		path: path.join(DIST_DIR, '/app'),
		filename: 'bundle.js',
		publicPath: '/app/'
	},
	devtool: 'inline-source-map',
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: [/node_modules/],
				use: [{
					loader: 'babel-loader'
				}]
			},
			{
				test: /\.less$/,
				use: ExtractTextPlugin.extract({
					fallback: "style-loader",
					use: [
						{
							loader: 'css-loader',
							options: { sourceMap: true, importLoaders: 1 }
						},
						{
							loader: 'less-loader',
							options: { sourceMap: true }
						}
					]
				})
			},
			{
				test: /\.(png|jpg|svg|ttf)$/,
				use: "file-loader?name=[name].[ext]"
			}
		]
	},
	plugins: [
		new ExtractTextPlugin('bundle.css')
	]
};
